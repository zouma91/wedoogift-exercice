# wedoogift-exercice

## Architecture diagram


![diagram](Diagram/diagram.png)


## Architecture explanation

To log the "brands" searched by users, I suggest to use cloudwatch service and cloudwatch agent component.
They are a native aws services that can be easily integrated on to the infrastructure with a few lines of configuration.
Cloudwatch has the Logs Insights functionality, very useful to create advanced log queries and get some statistics and that what we exactly need.

This a a detailed use case:
1. The user connects to the website and write a key word on the search zone and tap "Enter" key 
2. On the front-end side, the micro service will send a http call (get) to the search API located on the back-end side.
3. On the backhand side, we can edit the micro service code, which contains search service, to call a new function "SaveWordOnFile (String param)". This function will add the new searched word on a log file "words.log".
4. We install a cloudwatch agent on the container 
5. We create a new log group on cloudwatch with console or CLI 
6. We configure the cloudwatch agent to watch the "words.log" file and send the word list to the log group already created 
7. When there is a new modification on the file, The cloudwatch agent will synchronize the file content with the cloudwatch logGroup 
8. Now with the cloudCloudWatch Logs Insights aws service, we can write queries to get some statistics and create useful dashbords
